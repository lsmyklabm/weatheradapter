<?php

namespace WeatherAdapter\DependencyInjection;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\HttpKernel\DependencyInjection\Extension;
use Symfony\Component\DependencyInjection\Loader;

/**
 * Class WeatherAdapterExtension
 * @package WeatherAdapter\DependencyInjection
 */
class WeatherAdapterExtension extends Extension
{
    /**
     * @param array $configs
     * @param ContainerBuilder $container
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        if (isset($config['api_keys'])) {
            $container->setParameter('weather_adapter', $config);
            $container->setParameter('weather_adapter.api_keys', $config['api_keys']);
        }

        if (isset($config['default_provider'])) {
            $container->setParameter('weather_adapter.default_provider', $config['default_provider']);
        }

        $loader = new Loader\YamlFileLoader($container, new FileLocator(__DIR__ . '/../Resources/config'));
        $loader->load('services.yml');
    }

    /**
     * @return string
     */
    public function getAlias()
    {
        return 'weather_adapter';
    }
}
