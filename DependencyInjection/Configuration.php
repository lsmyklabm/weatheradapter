<?php

namespace WeatherAdapter\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 * @package WeatherAdapter\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @return TreeBuilder
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder();
        $rootNode = $treeBuilder->root('weather_adapter');
        $rootNode->children()
            ->arrayNode('api_keys')
                ->addDefaultsIfNotSet()
                ->children()
                    ->scalarNode('accuweather')->defaultValue('accuweather_key')->end()
                    ->scalarNode('openweathermap')->defaultValue('openweathermap_app_id')->end()
                ->end()
            ->end()
            ->scalarNode('default_provider')
                ->defaultValue('openweathermap')
            ->end();

        return $treeBuilder;
    }
}
