Weather Adapter
---------------

#### Installation
###### For SF4 based project only


The recommended way to install Weather Adapter is through
[Composer](http://getcomposer.org).

1. Install composer
2. Add ```"weather/weatheradapter": "1.*"``` to your ```composer.json```
3. Run ```composer update```
4. Activate Weather Adapter bundle in ```[project_dir]/config/bundles.php```
5. Copy  ```weather_adapter.yaml.dist``` to ```[project_dir]/config/packages/weather_adapter.yaml```
6. Add Accuweather Api Key as ```WEATHER_AW_API_KEY``` env variable
7. Add OpenWeatherMap App ID as ```WEATHER_OWM_API_APP_ID``` env variable
8. Done :)
 