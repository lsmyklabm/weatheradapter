<?php

namespace WeatherAdapter\Api;

/**
 * Class WeatherAdapter
 *
 * @package WeatherAdapter\Api
 */
class WeatherAdapter
{
    /**
     * @var OpenweathermapAdapter|AccuweatherAdapter
     */
    private $provider = null;

    /**
     * Prepare api key and weather api provider
     *
     * @param string $apiKey
     * @return void
     */
    public function settings(array $settings = []): void
    {
        $provider = $settings['default_provider'];
        
        /**
         *
         * Simple IF statement
         * TODO: Change if more providers needed
         *
         */
        if ($provider === 'openweathermap') {
            $this->provider = new OpenweathermapAdapter($settings['api_keys']['openweathermap']);
        } else {
            $this->provider = new AccuweatherAdapter($settings['api_keys']['accuweather']);
        }
    }

    /**
     * Return data about current weather conditions based on latitude and longitude
     *
     * @param float $lat
     * @param float $lon
     * @return array
     * @throws \Exception
     */
    public function getCurrentConditionsByGeoposition(float $lat, float $lon): array
    {
        return $this->provider->getCurrentConditionsByGeoposition($lat, $lon);
    }

    /**
     * Return data about current wetaher conditions based on zip code
     *
     * @param string $zipCode
     * @return array
     * @throws \Exception
     */
    public function getCurrentConditionsByZipCode(string $zipCode = ''): array
    {
        return $this->provider->getCurrentConditionsByZipCode($zipCode);
    }

    /**
     * Return data about 5-day forecast based on latitude and longitude
     *
     * @param float $lat
     * @param float $lon
     * @return array
     * @throws \Exception
     */
    public function getForecastByGeoposition(float $lat, float $lon): array
    {
        return $this->provider->getForecastByGeoposition($lat, $lon);
    }

    /**
     * Return data about 5-day forecast based on zip code
     *
     * @param string $zipCode
     * @return array
     * @throws \Exception
     */
    public function getForecastByZipCode(string $zipCode = ''): array
    {
        return $this->provider->getForecastByZipCode($zipCode);
    }
}
