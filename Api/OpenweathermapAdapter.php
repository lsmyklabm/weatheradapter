<?php

namespace WeatherAdapter\Api;

use Cmfcmf\OpenWeatherMap;
use Cmfcmf\OpenWeatherMap\Exception as OWMException;

/**
 * Class OpenweathermapAdapter
 *
 * @package WeatherAdapter\Api
 */
class OpenweathermapAdapter extends WeatherStandardResult
{
    /**
     * @var OpenWeatherMap $client
     */
    private $client = null;

    public function __construct(string $apiKey = '')
    {
        $this->client = new OpenWeatherMap($apiKey);
    }

    /**
     * Return data about current weather conditions based on latitude and longitude
     *
     * @param float $lat
     * @param float $lon
     * @return array
     * @throws \Exception
     */
    public function getCurrentConditionsByGeoposition(float $lat, float $lon): array
    {
        try {
            $weather = $this->client->getWeather(['lat' => $lat, 'lon' => $lon], $this->unitType);

            $this->observationPlace = $weather->city->name;
            $this->localObservationDateTime = $weather->lastUpdate->format('m/d/y H:i');
            $this->weatherIndicator = ucfirst($weather->weather->description);
            $this->weatherIcon = WeatherIcons::$openweathermapIcons[$weather->weather->icon] ?? 'wi-na';
            $this->humidity['Value'] = $weather->humidity->getValue();
            $this->temperature['Value'] = $weather->temperature->now->getValue();
            $this->precipitation['Value'] = $weather->precipitation->getValue();


        } catch (\Exception | OWMException $e) {

        }

        return $this->__toArray();
    }

    /**
     * Return data about current wetaher conditions based on zip code
     *
     * @param string $zipCode
     * @return array
     * @throws \Exception
     */
    public function getCurrentConditionsByZipCode(string $zipCode = ''): array
    {
        try {
            $weather = $this->client->getWeather('zip:' . $zipCode . ',US', $this->unitType);

            $this->observationPlace = $weather->city->name;
            $this->localObservationDateTime = $weather->lastUpdate->format('m/d/y H:i');
            $this->weatherIndicator = ucfirst($weather->weather->description);
            $this->weatherIcon = WeatherIcons::$openweathermapIcons[$weather->weather->icon] ?? 'wi-na';
            $this->humidity['Value'] = $weather->humidity->getValue();
            $this->temperature['Value'] = $weather->temperature->now->getValue();
            $this->precipitation['Value'] = $weather->precipitation->getValue();
            
        } catch (\Exception | OWMException $e) {

        }

        return $this->__toArray();
    }

    /**
     * Return data about 5-day forecast based on latitude and longitude
     *
     * @param float $lat
     * @param float $lon
     * @return array
     * @throws \Exception
     */
    public function getForecastByGeoposition(float $lat, float $lon): array
    {
        $forecast = [];

        try {
            $weather = $this->client->getWeatherForecast(['lat' => $lat, 'lon' => $lon], $this->unitType, '', '', 5);

            $forecast = $this->normalizeForecast($weather);
        } catch (\Exception | OWMException $e) {

        }

        return $forecast;
    }

    /**
     * Return data about 5-day forecast based on zip code
     *
     * @param string $zipCode
     * @return array
     * @throws \Exception
     */
    public function getForecastByZipCode(string $zipCode = ''): array
    {
        $forecast = [];

        try {
            $weather = $this->client->getWeatherForecast('zip:' . $zipCode . ',US', $this->unitType, '', '', 5);

            $forecast = $this->normalizeForecast($weather);
        } catch (\Exception | OWMException $e) {

        }

        return $forecast;
    }

    /**
     * Find data about next 5 day weather
     *
     * @param OpenWeatherMap\WeatherForecast $weatherForecast
     * @return array|null
     */
    private function normalizeForecast(OpenWeatherMap\WeatherForecast $weatherForecast): ?array
    {
        $tmp = [];
        $forecast = [];
        foreach ($weatherForecast as $wf) {
            $tmp[$wf->time->day->getTimestamp()][] = $wf;
        }

        foreach ($tmp as $timestamp => $weatherCollection) {
            $this->observationPlace = $weatherForecast->city->name;
            $this->localObservationDateTime = $weatherForecast->lastUpdate->format('m/d/y H:i');
            $temp = 0;
            $humidity = 0;
            $precipitation = 0;

            foreach ($weatherCollection as $weatherItem) {
                $temp += $weatherItem->temperature->now->getValue();
                $humidity += $weatherItem->humidity->getValue();
                $precipitation += $weatherItem->precipitation->getValue();
            }

            $quantity = count($tmp[$timestamp]);

            $this->humidity['Value'] = round(($humidity / $quantity), 2);
            $this->temperature['Value'] = round(($temp / $quantity), 2);
            $this->precipitation['Value'] = round(($precipitation / $quantity), 2);

            // Middle index should show noon weather
            $middleWeatherCollectionIndex = floor((count($weatherCollection) / 2));
            if (isset($weatherCollection[$middleWeatherCollectionIndex])) {
                $this->weatherIndicator = ucfirst($weatherCollection[$middleWeatherCollectionIndex]->weather->description);
                $this->weatherIcon = WeatherIcons::$openweathermapIcons[$weatherCollection[$middleWeatherCollectionIndex]->weather->icon] ?? 'wi-na';
            }

            $data = $this->__toArray();
            $data['Day'] = current($weatherCollection)->time->day->format('Y-m-d');

            $forecast[] = $data;
            $this->clear();
        }

        return $forecast;
    }
}
