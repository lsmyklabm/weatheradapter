<?php

namespace WeatherAdapter\Api;


/**
 * Class WeatherStandardResult
 * @package WeatherAdapter\Api
 */
abstract class WeatherStandardResult
{
    private static $units = array(
        'imperial' => [
            'temperature' => 'F',
            'precipitation' => 'in',
            'humidity' => '%'
        ],
        'metric' => [
            'temperature' => 'C',
            'precipitation' => 'mm',
            'humidity' => '%'
        ]
    );

    public $unitType = 'imperial';

    /**
     * @var string
     */
    protected $localObservationDateTime = '';
    /**
     * @var string
     */
    protected $observationPlace = '';
    /**
     * @var string
     */
    protected $weatherIndicator = '';
    /**
     * @var string
     */
    protected $weatherIcon = '';
    /**
     * @var int
     */
    protected $humidity = [
        'Value' => 0,
        'Unit' => ''
    ];
    /**
     * @var array
     */
    protected $temperature = [
        'Value' => 0,
        'Unit' => ''
    ];
    /**
     * @var array
     */
    protected $precipitation = [
        'Value' => 0,
        'Unit' => ''
    ];

    public function __toArray()
    {
        return [
            'LocalObservationDateTime' => $this->localObservationDateTime,
            'ObservationPlace' => $this->observationPlace,
            'WeatherIndicator' => $this->weatherIndicator,
            'WeatherIcon' => $this->weatherIcon,
            'Temperature' => [
                'Value' => $this->temperature['Value'],
                'Unit' => self::$units[$this->unitType]['temperature']
            ],
            'Humidity' => [
                'Value' => $this->humidity['Value'],
                'Unit' => self::$units[$this->unitType]['humidity']
            ],
            'Precipitation' => [
                'Value' => $this->precipitation['Value'],
                'Unit' => self::$units[$this->unitType]['precipitation']
            ]
        ];
    }

    protected function clear()
    {
        $this->localObservationDateTime = '';
        $this->observationPlace = '';
        $this->weatherIndicator = '';
        $this->weatherIcon = '';
        $this->humidity = ['Value' => 0, 'Unit' => 0];
        $this->temperature = ['Value' => 0, 'Unit' => 0];
        $this->precipitation = ['Value' => 0, 'Unit' => 0];
    }
}