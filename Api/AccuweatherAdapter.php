<?php

namespace WeatherAdapter\Api;

use GuzzleHttp\Client;
use function GuzzleHttp\Psr7\build_query;

/**
 * Class AccuweatherAdapter
 *
 * @package WeatherAdapter\Api
 */
class AccuweatherAdapter extends WeatherStandardResult
{
    /**
     * string API_BASE_URI
     */
    const API_BASE_URI = 'http://dataservice.accuweather.com/';

    /**
     * @var string $apiKey
     */
    private $apiKey = '';

    /**
     * @var Client $client
     */
    private $client = null;

    public function __construct(string $apiKey = '')
    {
        $this->apiKey = $apiKey;
        $this->client = new Client([
            'base_uri' => self::API_BASE_URI
        ]);
    }

    /**
     * Return data about current weather conditions based on latitude and longitude
     *
     * @param float $lat
     * @param float $lon
     * @return array
     * @throws \Exception
     */
    public function getCurrentConditionsByGeoposition(float $lat, float $lon): array
    {
        $latLonParam = $lat . ',' . $lon;
        $location = $this->getLocation(null, $latLonParam);

        return $this->getCurrentConditions($location);
    }

    /**
     * Return data about current wetaher conditions based on zip code
     *
     * @param string $zipCode
     * @return array
     * @throws \Exception
     */
    public function getCurrentConditionsByZipCode(string $zipCode = ''): array
    {
        $location = $this->getLocation($zipCode);

        return $this->getCurrentConditions($location);
    }

    /**
     * Return data about 5-day forecast based on latitude and longitude
     *
     * @param float $lat
     * @param float $lon
     * @return array
     * @throws \Exception
     */
    public function getForecastByGeoposition(float $lat, float $lon): array
    {
        $latLonParam = $lat . ',' . $lon;
        $location = $this->getLocation(null, $latLonParam);

        return $this->getForecast($location);
    }

    /**
     * Return data about 5-day forecast based on zip code
     *
     * @param string $zipCode
     * @return array
     * @throws \Exception
     */
    public function getForecastByZipCode(string $zipCode = ''): array
    {
        $location = $this->getLocation($zipCode);

        return $this->getForecast($location);
    }

    /**
     * Find data about next 5 day weather based location
     *
     * @param array $location
     * @return array
     * @throws \Exception
     */
    private function getForecast(array $location): ?array
    {
        $apiResource = 'forecasts/v1/daily/5day/' . $location['Key'];
        $forecast = [];

        $uri = $this->buildUri($apiResource, ['details' => 'true']);

        $weatherForecast = $this->client->get($uri);
        $response = $weatherForecast->getBody()->getContents();

        if ($response === 'null') {
            throw new \Exception('Forecast was not found!');
        }

        $decodedResponse = @json_decode($response, true);

        if (isset($decodedResponse['DailyForecasts'])) {
            $localObservationDateTime = new \DateTime();

            foreach ($decodedResponse['DailyForecasts'] as $day) {
                $this->observationPlace = $location['EnglishName'];
                $this->localObservationDateTime = $localObservationDateTime->format('m/d/y H:i');

                $averageTemp = ((float)$day['Temperature']['Minimum']['Value'] + (float)$day['Temperature']['Maximum']['Value']) / 2;
                $averageTemp = ((float)$day['Temperature']['Minimum']['Value'] + (float)$day['Temperature']['Maximum']['Value']) / 2;

                $this->temperature['Value'] = round($averageTemp, 2);
                $this->precipitation['Value'] = $day['Day']['TotalLiquid']['Value'];

                $this->weatherIndicator = $day['Day']['IconPhrase'];
                $this->weatherIcon = WeatherIcons::$accuweatherIcons[$day['Day']['Icon']] ?? 'wi-na';

                $data = $this->__toArray();
                $data['Day'] = (new \DateTime($day['Date']))->format('Y-m-d');

                $forecast[] = $data;
                $this->clear();
            }
        }

        return $forecast;
    }

    /**
     * Find data about current weather conditions based location key
     *
     * @param string $location
     * @return array
     * @throws \Exception
     */
    private function getCurrentConditions(array $location): ?array
    {
        $apiResource = 'currentconditions/v1/' . $location['Key'];
        $condition = null;

        $uri = $this->buildUri($apiResource, ['details' => 'true']);

        $weatherConditions = $this->client->get($uri);
        $response = $weatherConditions->getBody()->getContents();

        if ($response === 'null') {
            throw new \Exception('Weather conditions was not found!');
        }

        $decodedResponse = @json_decode($response, true);

        if (isset($decodedResponse[0])) {
            $this->observationPlace = $location['EnglishName'];
            $this->localObservationDateTime = (new \DateTime($decodedResponse[0]['LocalObservationDateTime']))->format('m/d/y H:i');
            $this->weatherIndicator = $decodedResponse[0]['WeatherText'];
            $this->weatherIcon = WeatherIcons::$accuweatherIcons[$decodedResponse[0]['WeatherIcon']] ?? 'wi-na';
            $this->humidity['Value'] = $decodedResponse[0]['RelativeHumidity'];
            $this->temperature['Value'] = $decodedResponse[0]['Temperature']['Imperial']['Value'];
            $this->precipitation['Value'] = $decodedResponse[0]['PrecipitationSummary']['Precipitation']['Imperial']['Value'];
        }

        return $this->__toArray();
    }

    /**
     * Return location key for given zip code or latitude and longitude
     *
     * @param string|null $zipCode
     * @param string|null $latLon
     * @return array|null
     * @throws \Exception
     */
    private function getLocation(string $zipCode = null, string $latLon = null): ?array
    {
        if ($zipCode === null && $latLon === null) {
            throw new \Exception('Zip code or geoposition is required!');
        }

        if ($zipCode == null) {
            $locationResource = 'locations/v1/cities/geoposition/search';
            $query = ['q' => $latLon];
        } else {
            $locationResource = 'locations/v1/search';
            $query = ['q' => $zipCode];
        }

        $uri = $this->buildUri($locationResource, $query);

        $location = $this->client->get($uri);
        $response = $location->getBody()->getContents();

        if ($response === 'null') {
            throw new \Exception('Location for given geoposition not found!');
        }

        $decodedResponse = @json_decode($response, true);

        return isset($decodedResponse['Key']) ? $decodedResponse : $decodedResponse[0];
    }

    /**
     * @param string $uri
     * @param array $params
     * @return string
     */
    private function buildUri(string $uri, array $params = []): string
    {
        $params['apikey'] = $this->apiKey;
        $query = build_query($params);

        $uri = $uri . '?' . $query;

        return $uri;
    }
}
