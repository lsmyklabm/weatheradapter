<?php

namespace WeatherAdapter\Api;

class WeatherIcons
{
    /**
     * Accuweather icon ids matched with 'weather-icons' css class
     *
     * @see: https://erikflowers.github.io/weather-icons/
     * @var array
     */
    public static $accuweatherIcons = [
        1 => 'wi-day-sunny',
        2 => 'wi-day-sunny-overcast',
        3 => 'wi-day-cloudy',
        4 => 'wi-day-cloudy',
        5 => 'wi-day-haze',
        6 => 'wi-cloud',
        7 => 'wi-cloud',
        8 => 'wi-cloudy',
        11 => 'wi-fog',
        12 => 'wi-showers',
        13 => 'wi-day-showers',
        14 => 'wi-day-showers',
        15 => 'wi-thunderstorm',
        16 => 'wi-day-sleet-storm',
        17 => 'wi-day-thunderstorm',
        18 => 'wi-rain',
        19 => 'wi-strong-wind',
        20 => 'wi-day-cloudy-windy',
        21 => 'wi-day-windy',
        22 => 'wi-snow',
        23 => 'wi-day-snow',
        24 => 'wi-hail',
        25 => 'wi-sleet',
        26 => 'wi-sprinkle',
        29 => 'wi-rain-mix',
        30 => 'wi-thermometer',
        31 => 'wi-thermometer-exterior',
        32 => 'wi-strong-wind',
        33 => 'wi-night-clear',
        34 => 'wi-night-alt-cloudy'
    ];

    /**
     * OpenWeatherMap icon ids matched with 'weather-icons' css class
     *
     * @see: https://erikflowers.github.io/weather-icons/
     * @var array
     */
    public static $openweathermapIcons = [
        '01d' => 'wi-day-sunny',
        '02d' => 'wi-day-sunny-overcast',
        '03d' => 'wi-day-cloudy',
        '04d' => 'wi-cloud',
        '09d' => 'wi-showers',
        '10d' => 'wi-showers',
        '11d' => 'wi-day-thunderstorm',
        '13d' => 'wi-day-snow',
        '50d' => 'wi-fog',
        '01n' => 'wi-night-clear',
        '02n' => 'wi-night-alt-cloudy',
        '03n' => 'wi-night-alt-cloudy',
        '04n' => 'wi-cloud',
        '09n' => 'wi-night-showers',
        '10n' => 'wi-night-showers',
        '11n' => 'wi-night-alt-lightning',
        '13n' => 'wi-night-alt-snow',
        '50n' => 'wi-night-fog'
    ];
}