<?php

namespace WeatherAdapter;

use WeatherAdapter\DependencyInjection\WeatherAdapterExtension;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class WeatherWeatherAdapterBundle
 * @package WeatherAdapter
 */
class WeatherWeatherAdapterBundle extends Bundle
{
    /**
     * @return WeatherAdapterExtension
     */
    public function getContainerExtension()
    {
        if (null === $this->extension) {
            $this->extension = new WeatherAdapterExtension();
        }

        return $this->extension;
    }
}
